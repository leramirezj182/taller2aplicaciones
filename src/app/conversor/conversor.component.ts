import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-conversor',
  templateUrl: './conversor.component.html',
  styleUrls: ['./conversor.component.css']
})
export class ConversorComponent implements OnInit {

  datoSend = { unidad_dato: "", unidad_conversion: "", dato: "" };
  datoReceived: any;

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }
  
  Convertir() {
    this.rest.doConversion(this.datoSend).subscribe((result) => {
     // this.productData = result;
      this.datoReceived = result;
      //this.router.navigate(['/product-details/'+result._id]);
    }, (err) => {
      console.log(err);
    });
    
  }



}
