#crear la imagen remota desde dckerhub
FROM node
#crear carpeta del proyecto
RUN mkdir -p /src/app
#espacio de trabajo
WORKDIR /src/app
COPY . .
RUN npm install -g @angular/cli
RUN npm install
EXPOSE 4200
CMD ng serve --host 0.0.0.0 --disable-host-check
